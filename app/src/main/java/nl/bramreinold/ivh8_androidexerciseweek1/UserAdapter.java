package nl.bramreinold.ivh8_androidexerciseweek1;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by bram_ on 7-5-2016.
 * UserAdapter is used to describe the translation between the data item and the View.
 */
public class UserAdapter extends BaseAdapter {
    // Attributes
    Context mContext;               // activity instance
    LayoutInflater mInflator;       // XML item layout
    ArrayList<User> mUserList;      // Model of user objects

    public UserAdapter(Context context, LayoutInflater layoutInflater, ArrayList<User> list) {
        this.mContext = context;
        this.mInflator = layoutInflater;
        this.mUserList = list;
    }

    @Override
    public int getCount() {
        int size = mUserList.size();
        Log.i("getCount", "=" + size);
        return size;
    }

    @Override
    public Object getItem(int position) {
        return mUserList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    /**
     * GetView
     * Is used to returns the actuel view as a row within the ListView at a particular position
     */
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder viewHolder;

        if(convertView == null) {
            convertView = mInflator.inflate(R.layout.activity_list_item, null);

            viewHolder = new ViewHolder();
            viewHolder.imageView = (ImageView) convertView.findViewById(R.id.UserImageView);
            viewHolder.name = (TextView) convertView.findViewById(R.id.userNameTextView);
            viewHolder.email = (TextView) convertView.findViewById(R.id.userEmailTextView);

            convertView.setTag(viewHolder);

        } else {
            // getTag the Object stored in this view as a tag, or if not set
            viewHolder = (ViewHolder) convertView.getTag();
        }

        // setup viewHolder with correct data
        User user = mUserList.get(position);

        viewHolder.name.setText(user.getName());
        // Check if user image isset
        if(user.getImage() != null) {
            // Set own image
            // Convert byte array to bitmap (necessary because we cannot send bitmap with intent)
            Bitmap image = BitmapFactory.decodeByteArray(user.getImage(), 0, user.getImage().length);
            viewHolder.imageView.setImageBitmap(image);
        } else {
            // Set default image
            Bitmap image = BitmapFactory.decodeResource(mContext.getResources(), R.drawable.no_user);
            viewHolder.imageView.setImageBitmap(image);
        }
        viewHolder.email.setText(user.getEmail());

        return convertView;
    }

    private static class ViewHolder {
        public ImageView imageView;
        public TextView name;
        public TextView email;
    }
}
