package nl.bramreinold.ivh8_androidexerciseweek1;

import android.content.Intent;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements AdapterView.OnItemClickListener {

    private ListView mUserListView;
    private UserAdapter mUserAdapter;
    private ArrayList<User> mUserList = new ArrayList();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);

        // Create some Users
        User user1 = new User("Bram Reinold", "bram_reinold@hotmail.com");
        user1.setImage(BitmapFactory.decodeResource(getResources(), R.drawable.user1 ) );
        user1.setGender("Man");
        user1.setResidence("Made");
        user1.setStreet("Nieuwstraat");
        user1.setHouseNr("37");
        user1.setZipcode("3322DD");
        user1.setPhoneNr("686422");
        user1.setMobileNr("0655443366");
        user1.setNationality("Nederlandse");
        user1.setUsername("Brambooo");
        user1.setPassword("123123123");
        mUserList.add(user1);

        User user2 = new User("Bart Jansen", "bart_jansen@gmail.com" );
        user1.setGender("Man");
        user1.setResidence("Breda");
        user1.setStreet("Nieuwelaan");
        user1.setHouseNr("55");
        user1.setZipcode("3322DD");
        user1.setPhoneNr("686422");
        user1.setMobileNr("0655443366");
        user1.setNationality("Nederlandse");
        user1.setUsername("Brambooo");
        user1.setPassword("123123123");
        mUserList.add(user2);

        User user3 = new User("Kees kroket", "keeskroket@gmail.com" );
        user1.setGender("Man");
        user1.setResidence("Breda");
        user1.setStreet("Nieuwelaan");
        user1.setHouseNr("55");
        user1.setZipcode("3322DD");
        user1.setPhoneNr("686422");
        user1.setMobileNr("0655443366");
        user1.setNationality("Nederlandse");
        user1.setUsername("Brambooo");
        user1.setPassword("123123123");
        mUserList.add(user3);

        User user4 = new User("Paul Drawl", "pauldrwl@gmail.com" );
        user1.setGender("Man");
        user1.setResidence("Breda");
        user1.setStreet("Nieuwelaan");
        user1.setHouseNr("55");
        user1.setZipcode("3322DD");
        user1.setPhoneNr("686422");
        user1.setMobileNr("0655443366");
        user1.setNationality("Nederlandse");
        user1.setUsername("Brambooo");
        user1.setPassword("123123123");
        mUserList.add(user4);

        User user5 = new User("Henk Fietsbel", "henkfietsbel@gmail.com" );
        user1.setGender("Man");
        user1.setResidence("Breda");
        user1.setStreet("Nieuwelaan");
        user1.setHouseNr("55");
        user1.setZipcode("3322DD");
        user1.setPhoneNr("686422");
        user1.setMobileNr("0655443366");
        user1.setNationality("Nederlandse");
        user1.setUsername("Brambooo");
        user1.setPassword("123123123");
        mUserList.add(user5);

        User user6 = new User("Willem Jansen", "willem_jansen@gmail.com" );
        user1.setGender("Man");
        user1.setResidence("Breda");
        user1.setStreet("Nieuwelaan");
        user1.setHouseNr("55");
        user1.setZipcode("3322DD");
        user1.setPhoneNr("686422");
        user1.setMobileNr("0655443366");
        user1.setNationality("Nederlandse");
        user1.setUsername("Brambooo");
        user1.setPassword("123123123");
        mUserList.add(user6);


        // Get the user list view
        mUserListView = (ListView) findViewById(R.id.userListView);

        // Couple list to adapter
        mUserAdapter = new UserAdapter(this, getLayoutInflater(), mUserList);
        mUserListView.setAdapter(mUserAdapter);

        // Enable click listener
        mUserListView.setOnItemClickListener(this);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

        // Here we use an intent to navigate to a new activity (the detailpage)
        // Initialize intent
        Intent i = new Intent(getApplicationContext(), DetailActivity.class);

        User user = mUserList.get(position);

//        // Get image
//        byte[] imageByteArray[] = user.getImage();
        i.putExtra("USER", user);

        Log.d("Gekozen item: ", position + "");

        // Go to other activity
        startActivity(i);
    }

}
