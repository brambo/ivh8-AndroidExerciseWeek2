package nl.bramreinold.ivh8_androidexerciseweek1;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by Bram on 7-5-2016.
 */
public class DetailActivity extends AppCompatActivity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // set view
        setContentView(R.layout.activity_detail);

        // Get user object through serializable
        User user = (User) getIntent().getSerializableExtra("USER");
        // convert byte array to bitmap
        // Only set image when user got an image
        if(user.getImage() != null) {
            Bitmap image = BitmapFactory.decodeByteArray(user.getImage(), 0, user.getImage().length);
            ImageView imageView = (ImageView) findViewById(R.id.imageViewPersonIcon) ;
            imageView.setImageBitmap(image);
        }

        TextView tvName = (TextView) findViewById(R.id.textViewPersonTitle);
        tvName.setText(user.getName());

        TextView tvGender = (TextView) findViewById(R.id.textViewInfoGender);
        tvGender.setText(user.getGender());

        TextView tvStreet = (TextView) findViewById(R.id.textViewInfoStreet);
        tvStreet.setText(user.getStreet());

        TextView tvResidence = (TextView) findViewById(R.id.textViewInfoCity);
        tvResidence.setText(user.getResidence());

        TextView tvZipcode = (TextView) findViewById(R.id.textViewZipcodeInfo);
        tvZipcode.setText(user.getZipcode());

        TextView tvState = (TextView) findViewById(R.id.textViewTitleStateInfo);
        tvState.setText(user.getState());

        TextView tvMobile = (TextView) findViewById(R.id.textViewMobileInfo);
        tvMobile.setText(user.getMobileNr());

        TextView tvPhone = (TextView) findViewById(R.id.textViewPhoneInfo);
        tvPhone.setText(user.getPhoneNr());

        TextView tvNationality = (TextView) findViewById(R.id.textViewNationalityInfo);
        tvNationality.setText(user.getNationality());

        TextView tvUsername = (TextView) findViewById(R.id.textViewUsernameInfo);
        tvUsername.setText(user.getUsername());

        TextView tvEmail = (TextView) findViewById(R.id.textViewEmailInfo);
        tvEmail.setText(user.getEmail());

        TextView tvPassword = (TextView) findViewById(R.id.textViewPasswordInfo);
        tvPassword.setText(user.getPassword());

    }
}
