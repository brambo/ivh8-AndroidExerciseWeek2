package nl.bramreinold.ivh8_androidexerciseweek1;

import android.graphics.Bitmap;

import java.io.ByteArrayOutputStream;
import java.io.Serializable;

/**
 * Created by bram_ on 7-5-2016.
 */
public class User implements Serializable {
    // Attributes
    private String name;
    private int age;
    private String gender;
    private String street;
    private String residence;
    private String houseNr;
    private String state;
    private String zipcode;
    private String phoneNr;
    private String mobileNr;
    private String nationality;

    private String username;
    private String email;
    private String password;
    private byte[] image;

    // Constructor
    public User(String name, String email) {
        this.name = name;
        this.email = email;
    }

    // Methods
    public String getName() {
        if(name != null) {
            return name;
        } else {
            return "-";
        }
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getGender() {
        if(gender != null) {
            return gender;
        } else {
            return "-";
        }
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getStreet() {
        if(street != null) {
            return street;
        } else {
            return "-";
        }
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getResidence() {
        if(residence != null) {
            return residence;
        } else {
            return "-";
        }
    }

    public void setResidence(String residence) {
        this.residence = residence;
    }

    public String getHouseNr() {
        if(houseNr != null) {
            return houseNr;
        } else {
            return "-";
        }
    }

    public void setHouseNr(String houseNr) {
        this.houseNr = houseNr;
    }

    public String getState() {
        if(state != null) {
            return state;
        } else {
            return "-";
        }
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getPhoneNr() {
        if(phoneNr != null) {
            return phoneNr;
        } else {
            return "-";
        }
    }

    public void setPhoneNr(String phoneNr) {
        this.phoneNr = phoneNr;
    }

    public String getMobileNr() {
        if(mobileNr != null) {
            return mobileNr;
        } else {
            return "-";
        }
    }

    public void setMobileNr(String mobileNr) {
        this.mobileNr = mobileNr;
    }

    public String getNationality() {
        if(nationality != null) {
            return nationality;
        } else {
            return "-";
        }
    }

    public void setNationality(String nationality) {
        this.nationality = nationality;
    }

    public String getUsername() {
        if(username != null) {
            return username;
        } else {
            return "-";
        }
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        if(email != null) {
            return email;
        } else {
            return "-";
        }
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        if(password != null) {
            return password;
        } else {
            return "-";
        }
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public byte[] getImage() {
        return image;
    }

    public void setImage(Bitmap image) {
        // Only set if image is set else do nothing
        if(image != null) {
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            image.compress(Bitmap.CompressFormat.PNG, 0, byteArrayOutputStream);
            this.image = byteArrayOutputStream.toByteArray();
        }
    }

    public String getZipcode() {
        if(zipcode != null) {
            return zipcode;
        } else {
            return "-";
        }
    }

    public void setZipcode(String zipcode) {
        this.zipcode = zipcode;
    }
}
